package com.example.al.preferences;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity implements View.OnClickListener {

    EditText editName;
    EditText editYear;
    SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editName = (EditText)findViewById(R.id.editName);
        editYear = (EditText)findViewById(R.id.editYear);
        sharedPref = getPreferences(Context.MODE_PRIVATE); //MODE_WORLD_READABLE MODE_WORLD_WRITEABLE

        findViewById(R.id.buttonSave).setOnClickListener(this);
        findViewById(R.id.buttonLoad).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonSave:
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("name", editName.getText().toString());
                editor.putInt("year", Integer.parseInt(editYear.getText().toString()));
                editor.apply();
                break;
            case R.id.buttonLoad:
                editName.setText(sharedPref.getString("name", ""));
                editYear.setText(String.valueOf(sharedPref.getInt("year", 0)));
                break;
        }
    }
}
